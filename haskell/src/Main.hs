{-# LANGUAGE
    OverloadedStrings
  , OverloadedLabels
  , DeriveGeneric
  , ScopedTypeVariables
  , RankNTypes
  , ExtendedDefaultRules
  , GeneralizedNewtypeDeriving
  , BangPatterns
#-}


module Main where

import qualified Prelude
import           Prelude               hiding (div)
import           GHC.Generics                 (Generic)
import           Data.Generics.Labels         ()
import           Data.Text                    (Text, pack)
import           Data.Function                ((&))
import           Data.Vector                  (Vector, fromList)
import qualified Data.Vector               as V
import qualified Data.Vector.Mutable       as VM
import           Data.STRef                   (newSTRef, readSTRef, modifySTRef', writeSTRef)
import           Control.DeepSeq              (NFData (rnf))
import           Control.Lens                 ((%~), (.~), (^.), (?~))
import           Control.Lens.Indexed         (imap)
import           Control.Monad                (replicateM_, replicateM)
import           Control.Monad.IO.Class       (MonadIO, liftIO)
import           Control.Monad.ST             (runST)
import           System.Random                (StdGen, getStdGen, randomR)
-- import           System.Random.Stateful       (randomRM, runSTGen)
import           Shpadoinkle                  (Html, JSM)
import           Shpadoinkle.Backend.ParDiff  (runParDiff)
import           Shpadoinkle.Backend.Snabbdom (runSnabbdom)
import           Shpadoinkle.Html             ( span', textProperty, class', getById, getBody, onClick, onClickM
                                              , id', type', button, td, tr, text, div, a, h1_, table, tbody_)
import           Shpadoinkle.Run              (live, runJSorWarp, simple)


-- mostly ripped from https://github.com/krausest/js-framework-benchmark/blob/master/frameworks/keyed/react/src/main.jsx


adjectives :: Vector Text
adjectives = fromList
  [ "pretty", "large", "big", "small", "tall", "short", "long", "handsome", "plain", "quaint", "clean"
  , "elegant", "easy", "angry", "crazy", "helpful", "mushy", "odd", "unsightly", "adorable", "important"
  , "inexpensive", "cheap", "expensive", "fancy"]
colors :: Vector Text
colors = fromList
  ["red", "yellow", "blue", "green", "pink", "brown", "purple", "brown", "white", "black", "orange"]
nouns :: Vector Text
nouns = fromList
  [ "table", "chair", "house", "bbq", "desk", "car", "pony", "cookie", "sandwich", "burger", "pizza"
  , "mouse", "keyboard"]


newtype Ident = Ident Int
  deriving (Eq, Show, Read, Num, NFData)


data Row = Row
  { buildDataId       :: !Ident
  , buildDataLabel    :: {-# UNPACK #-} !Text
  , buildDataSelected :: {-# UNPACK #-} !Text
  } deriving (Generic, Eq, Show, Read)

instance NFData Row

newRow :: Ident -> Text -> Row
newRow ident label = Row ident label ""


instance Eq StdGen where
  _ == _ = True

data State = State
  { stateData     :: [Row]
  , stateSelected :: !(Maybe Int)
  , stateNewIdent :: {-# UNPACK #-} !Ident
  , stateStdGen   :: {-# UNPACK #-} !StdGen -- FIXME just use randomRIO
  } deriving (Generic, Eq, Show, Read)

instance NFData State where
  rnf (State ds s !_ !_) = rnf ds `seq` rnf s

initialState :: StdGen -> State
initialState = State [] Nothing 1


-- | Generates a set of random combinations of an adjective, color, and noun.
generateLabels :: Int -> StdGen -> ([Text], StdGen)
generateLabels count g = runST $ do
  gRef <- newSTRef g
  xs <- replicateM count (randomBuildData gRef)
  g' <- readSTRef gRef
  pure (xs, g')
  where
    -- randomBuildData :: STRef s StdGen -> ST s Text
    randomBuildData gRef = do
      (\a' c' n' -> a' <> " " <> c' <> " " <> n')
        <$> getRandomElement gRef adjectives
        <*> getRandomElement gRef colors
        <*> getRandomElement gRef nouns
    -- getRandomElement :: STRef s StdGen -> Vector Text -> ST s Text
    getRandomElement gRef xs = (xs V.!) <$> randomRM (0, length xs - 1) gRef
    -- randomRM :: (Int, Int) -> STRef s StdGen -> ST s Int
    randomRM xs gRef = do
      g' <- readSTRef gRef
      let (x, g'') = randomR xs g'
      writeSTRef gRef g''
      pure x

-- | Button with an event handler when clicked
button' :: Applicative m => Text -> (a -> a) -> Text -> Html m a
button' id'' cb title =
  div [class' ["col-sm-6", ("smallpad" :: Text)]]
    [ button
      [ type' "button"
      , class' ["btn", "btn-primary", ("btn-block" :: Text)]
      , id' id''
      , onClick cb
      ]
      [text title]
    ]

-- | Given a previous state and a count, create a new state and a set of rows.
makeBuildData :: Int -> State -> (State, [Row])
makeBuildData count state =
  let
    (ls, g') = generateLabels count (state ^. #stateStdGen)
  in
    ( state
        & #stateNewIdent %~ (+ fromIntegral count)
        & #stateStdGen .~ g'
    , let firstIdent = state ^. #stateNewIdent
          go :: Int -> Text -> Row
          go idx l = newRow (fromIntegral idx + firstIdent) l
      in  imap go ls
    )


view :: forall m. Applicative m => State -> Html m State
view (State ds sel _ _) =
  div "container"
    [ jumbotron
    , table [class' ["table", "table-hover", "table-striped", ("test-data" :: Text)]]
      -- All rows
      [ tbody_ (imap row ds)
      ]
    , span'
      [ class' ["preloadicon", "glyphicon", ("glyphicon-remove" :: Text)]
      , textProperty "aria-hidden" True
      ]
    ]
  where
    -- | View a single row
    row :: Int -> Row -> Html m State
    row idx (Row ident label selected) =
      tr [class' selected]
        [ td "col-md-1" [text . pack . show $ ident]
        , td "col-md-4" [a [onClick (select idx)] [text label]]
        , td "col-md-1"
          [ a [onClick (remove idx)]
            [ span' [class' ["glyphicon", ("glyphicon-remove" :: Text)], textProperty "aria-hidden" True]
            ]
          ]
        , td "col-md-6" []
        ]

    -- | All buttons
    jumbotron :: Html m State
    jumbotron =
      div "jumbotron"
        [ div "row"
          [ div "col-md-6" [h1_ ["Shpadoinkle"]]
          , div "col-md-6"
            [ div "row"
              [ button' "run" run "Create 1,000 rows"
              , button' "runlots" runLots "Create 10,000 rows"
              , button' "add" add "Append 1,000 rows"
              , button' "update" update "Update every 10th row"
              , button' "clear" clear "Clear"
              , button' "swaprows" swapRows "Swap Rows"
              ]
            ]
          ]
        ]


    run :: State -> State
    run state =
      let (state', xs) = makeBuildData 1000 state
      in  state' & #stateData .~ xs

    runLots :: State -> State
    runLots state =
      let (state', xs) = makeBuildData 10000 state
      in  state' & #stateData .~ xs

    add :: State -> State
    add state =
      let (state', xs) = makeBuildData 1000 state
      in  state' & #stateData %~ (<> xs)

    update :: State -> State
    update =
      #stateData %~ go
      where
        go :: [Row] -> [Row]
        go [] = []
        go xs =
          let toUpdate:pfx = take 10 xs
              sfx = drop 10 xs
              updated = toUpdate & #buildDataLabel %~ (<> " !!!")
          in  (updated : pfx) <> go sfx
        -- go :: Vector Row -> Vector Row
        -- go ds
        --   | dsLen == 0 = ds
        --   | otherwise = runST $ do
        --       dsRef <- V.thaw ds
        --       idxRef <- newSTRef 0 -- row getting updated
        --       let numTimes = dsLen `Prelude.div` 10 -- number of updates
        --       replicateM_ numTimes $ do
        --         idx <- readSTRef idxRef
        --         VM.unsafeModify dsRef (#buildDataLabel %~ (<> "!!!")) idx
        --         modifySTRef' idxRef (+ 10)
        --       V.freeze dsRef
        --   where
        --     -- length of rows
        --     dsLen :: Int
        --     dsLen = V.length ds

    -- FIXME could be optimized by different data type? IntMap Int?
    remove :: Int -> State -> State
    remove idx = #stateData %~ go -- V.filter (\(Row i' _) -> i' /= i)
      where
        go ds =
          let pfx = take idx ds
              sfx = drop (idx + 1) ds
              -- (ds', ds'') = V.splitAt idx ds
          in  pfx <> sfx -- ds' <> V.drop 1 ds''

    clear :: State -> State
    clear state = state & #stateData .~ [] & #stateSelected .~ Nothing

    swapRows :: State -> State
    swapRows = #stateData %~ go
      where
        go ds
          | length ds < 998 = ds
          | otherwise =
            let (i:x:pfx) = ds
                pfx' = take 996 pfx
                (y:sfx) = drop 996 pfx
            in  (i:y:pfx') <> (x:sfx)

      -- state@(State ds _ _ _)
      -- | length ds > 998 =
      --   let ds' = runST $ do
      --         dsRef <- V.thaw ds
      --         VM.unsafeSwap dsRef 1 998
      --         V.freeze dsRef
      --   in  state & #stateData .~ ds'
      -- | otherwise = state

    select :: Int -> State -> State
    select i state =
      case state ^. #stateSelected of
        Nothing ->
          let go :: [Row] -> [Row]
              go ds =
                let pfx = take i ds
                    (x:sfx) = drop i ds
                    x' = x & #buildDataSelected .~ "danger"
                in  pfx <> (x':sfx)
                -- runST $ do
                -- dsRef <- V.thaw ds
                -- VM.unsafeModify dsRef (#buildDataSelected .~ "danger") i
                -- V.freeze dsRef
          in  state & #stateSelected ?~ i
                    & #stateData %~ go
        Just i' ->
          let go :: [Row] -> [Row]
              go ds
                | i' == i = ds -- remain selected
                | i' < i =
                  let pfx = take i' ds
                      (old:sfx) = drop i' ds
                      pfx' = take (i - i' - 1) sfx
                      (new:sfx') = drop (i - i' - 1) sfx
                      old' = old & #buildDataSelected .~ ""
                      new' = new & #buildDataSelected .~ "danger"
                  in  pfx <> (old':pfx') <> (new':sfx')
                | i < i' =
                  let pfx = take i ds
                      (new:sfx) = drop i ds
                      pfx' = take (i' - i - 1) sfx
                      (old:sfx') = drop (i' - i - 1) sfx
                      old' = old & #buildDataSelected .~ ""
                      new' = new & #buildDataSelected .~ "danger"
                  in  pfx <> (new':pfx') <> (old':sfx')
                -- runST $ do
                -- dsRef <- V.thaw ds
                -- VM.unsafeModify dsRef (#buildDataSelected .~ "") i'
                -- VM.unsafeModify dsRef (#buildDataSelected .~ "danger") i
                -- V.freeze dsRef
          in  state & #stateSelected ?~ i
                    & #stateData %~ go


app :: JSM ()
app = do
  g <- getStdGen
  simple
    runParDiff
    -- runSnabbdom
    (initialState g)
    view
    -- getBody
    (getById "main")


port :: Int
port = 8080

dev :: IO ()
dev = live port app

main :: IO ()
main = do
  putStrLn $ "\nHappy point of view on http://localhost:" <> show port
  runJSorWarp port app
