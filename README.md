# Shpaboinchkle - Benchmark for Shpadoinkle

[Shpadoinkle](https://shpadoinkle.org) is a front-end UI framework, written
in [Haskell](https://haskell.org), using [GHCJS](https://github.com/ghcjs/ghcjs)
for browser execution.

## Prerequisites

There is only one prerequisite - [nix](https://nixos.org) must be installed on your
UNIX-like machine. The easiest way to install is with this command:

```bash
curl -L https://nixos.org/nix/install | sh
```

Also, it may improve your build time by using the Shpadoinkle [Cachix](https://cachix.org/)
builds:

```bash
nix-env -iA cachix -f https://cachix.org/api/v1/install
cachix use shpadoinkle
```

## Building

```bash
./build.sh
```

This will populate `result/bin/shpaboinchkle.jsexe/` (because it's actually a symlink to your
immutable nix build output), but more importantly save the new build to `js/`.
